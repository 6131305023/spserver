const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const mysql = require('mysql');
 
// parse application/json
app.use(bodyParser.json());
 
//create database connection
const conn = mysql.createConnection({
  host: 'se.mfu.ac.th',
  user: 'spmediseen',
  password: 'M3dI$eEn',
  database: 'sp_mediseen_db'
});
 
//connect to database
conn.connect((err) =>{
  if(err) throw err;
  console.log('Mysql Connected...');
});
 
//show all products
app.get('/api/Medicine',(req, res) => {
  let sql = "SELECT * FROM Medicine";
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
  });
});
 
//show single product
app.get('/api/Medicine/:id',(req, res) => {
  let sql = "SELECT * FROM Medicine WHERE med_ID="+req.params.id;
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
  });
});
 
//add new product
app.post('/api/Medicine',(req, res) => {
  let data = {med_GName: req.body.med_GName, med_USname : req.body.med_USname};
  let sql = "INSERT INTO Medicine SET ?";
  let query = conn.query(sql, data,(err, results) => {
    if(err) throw err;
    res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
  });
});
 
//update product
app.put('/api/Medicine/:id',(req, res) => {
  let sql = "UPDATE Medicine SET med_GName='"+req.body.med_GName+"', med_USname='"+req.body.med_USname+"' WHERE med_ID="+req.params.id;
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
    res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
  });
});
 
//Delete product
app.delete('/api/Medicine/:id',(req, res) => {
  let sql = "DELETE FROM Medicine WHERE med_ID="+req.params.id+"";
  let query = conn.query(sql, (err, results) => {
    if(err) throw err;
      res.send(JSON.stringify({"status": 200, "error": null, "response": results}));
  });
});
 
//Server listening
app.listen(3000,() =>{
  console.log('Server started on port 3000...');
});